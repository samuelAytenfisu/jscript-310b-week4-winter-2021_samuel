/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const suit = ["hearts", "diamonds", "clubs", "spades"];
const cardValues = [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11];
const cardDisplayValues = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"];
function getDeck()
{
	var deck = new Array();

	for(var i = 0; i < suit.length; i++)
	{
		for(var x = 0; x < cardValues.length; x++)
		{
			var card = {Val: cardValues[x], Suit: suit[i],displayVal:cardDisplayValues[x]};
			deck.push(card);
		}
	}

	return deck;
}



// // CHECKS
 const deck = getDeck();
 console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.Val && typeof randomCard.Val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.Suit && typeof randomCard.Suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard && randomCard.displayVal && typeof randomCard.displayVal ==='string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);